package com.epam.tp.training.dao;

import com.epam.tp.training.dao.domain.Training;

/**
 * Data access object for trainings.
 */
public interface TrainingDao {

    /**
     * Query all the trainings.
     *
     * @return the trainings
     */
    Iterable<Training> getTrainings();

    /**
     * Add a new training.
     *
     * @param training training
     */
    void addTraining(Training training);

    /**
     * Get the specified training.
     *
     * @param id id
     * @return the training
     */
    Training getTraining(Long id);

    /**
     * Modifie the specified training.
     *
     * @param id id
     * @param name name to set
     * @param description description to set
     * @param version 
     */
    void modifyTraining(Long id, String name, String description, long version);

}
