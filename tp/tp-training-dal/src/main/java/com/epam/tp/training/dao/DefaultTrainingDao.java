package com.epam.tp.training.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.tp.training.dao.domain.Training;
import com.epam.tp.training.dao.transformer.TrainingEntityToTrainingTransformer;
import com.epam.tp.training.dao.transformer.TrainingToTrainingEntityTransformer;
import com.epam.tp.training.entity.TrainingEntity;
import com.epam.tp.training.repository.TrainingRepository;

@Component
class DefaultTrainingDao implements TrainingDao {

    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private TrainingEntityToTrainingTransformer trainingEntityToTrainingTransformer;

    @Autowired
    private TrainingToTrainingEntityTransformer trainingToTrainingEntityTransformer;

    @Override
    public void addTraining(Training training) {
        TrainingEntity trainingEntity = trainingToTrainingEntityTransformer.transform(training);
        trainingRepository.save(trainingEntity);
    }

    @Override
    public Iterable<Training> getTrainings() {
        Iterable<TrainingEntity> trainingEntitys = trainingRepository.findByDeletedAtIsNull();
        List<Training> result = transform(trainingEntitys);
        return result;
    }

    private List<Training> transform(Iterable<TrainingEntity> trainingEntitys) {
        List<Training> result = new ArrayList<Training>();
        for (TrainingEntity trainingEntity : trainingEntitys) {
            result.add(trainingEntityToTrainingTransformer.transform(trainingEntity));
        }
        return result;
    }

    @Override
    public Training getTraining(Long id) {
        TrainingEntity training = trainingRepository.findOne(id);
        if (training == null) {
            throw new IllegalArgumentException("No training with this id: " + id);
        }
        return trainingEntityToTrainingTransformer.transform(training);
    }

    @Override
    public void modifyTraining(Long id, String name, String description, long version) {
        TrainingEntity training = trainingRepository.findOne(id);
        training.setName(name);
        training.setDescription(description);
        if(training.getVersion()!=version) {
        	throw new IllegalStateException();
        }
    }

}
