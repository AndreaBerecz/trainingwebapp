package com.epam.tp.training.dao.transformer;

import com.epam.tp.training.dao.domain.Training;
import com.epam.tp.training.entity.TrainingEntity;

/**
 * Transform TrainingEntity to Training.
 */
public interface TrainingEntityToTrainingTransformer {

    /**
     * Transformation.
     * @param trainingEntity to transform
     * @return transformed entity
     */
    Training transform(TrainingEntity trainingEntity);

}
