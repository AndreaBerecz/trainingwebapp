package com.epam.tp.training.dao.transformer;

import org.springframework.stereotype.Component;

import com.epam.tp.training.dao.domain.Training;
import com.epam.tp.training.entity.TrainingEntity;

@Component
class DefaultTrainingToTrainingEntityTransformer implements TrainingToTrainingEntityTransformer {

    @Override
    public TrainingEntity transform(Training training) {
        TrainingEntity trainingEntity = new TrainingEntity();
        trainingEntity.setId(training.getId());
        trainingEntity.setLength(training.getLength());
        trainingEntity.setName(training.getName());
        trainingEntity.setDescription(training.getDescription());
        trainingEntity.setDeletedAt(training.getDeletedAt());
        return trainingEntity;
    }

}
