package com.epam.tp.training.dao.transformer;

import org.springframework.stereotype.Component;

import com.epam.tp.training.dao.domain.Training;
import com.epam.tp.training.entity.TrainingEntity;

@Component
class DefaultTrainingEntityToTrainingTransformer implements TrainingEntityToTrainingTransformer {

    @Override
    public Training transform(TrainingEntity trainingEntity) {
        Training training = new Training();
        training.setCreatedAt(trainingEntity.getCreatedAt());
        training.setDeletedAt(trainingEntity.getDeletedAt());
        training.setDescription(trainingEntity.getDescription());
        training.setId(trainingEntity.getId());
        training.setLength(trainingEntity.getLength());
        training.setName(trainingEntity.getName());
        training.setVersion(trainingEntity.getVersion());
        return training;
    }

}
