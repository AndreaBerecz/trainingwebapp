package com.epam.tp.training.dao.transformer;

import com.epam.tp.training.dao.domain.Training;
import com.epam.tp.training.entity.TrainingEntity;

/**
 * Transform Training to TrainingEntity.
 */
public interface TrainingToTrainingEntityTransformer {

    /**
     * Transform.
     * @param training training to transform
     * @return transformed training
     */
    TrainingEntity transform(Training training);

}
