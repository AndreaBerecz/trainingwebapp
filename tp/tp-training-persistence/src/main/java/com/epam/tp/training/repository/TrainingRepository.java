package com.epam.tp.training.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.epam.tp.training.entity.TrainingEntity;

/**
 * Repository for trainings.
 */
public interface TrainingRepository extends CrudRepository<TrainingEntity, Long> {

    /**
     * Find existing trainings.
     * @return trainings
     */
    List<TrainingEntity> findByDeletedAtIsNull();

}
