package com.epam.tp.training.service;

import com.epam.tp.training.dao.domain.Training;

public interface TrainingService {

	Iterable<Training> getTrainings();

	void createDummyTrainings(int count);

	Training getTraining(Long id);

	void modifyTraining(Long id, String name, String description, long version);

}
