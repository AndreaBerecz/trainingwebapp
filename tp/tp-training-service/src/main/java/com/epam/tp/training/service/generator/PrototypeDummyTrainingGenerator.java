package com.epam.tp.training.service.generator;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.epam.tp.training.dao.domain.Training;

@Component
@Qualifier("prototype")
public class PrototypeDummyTrainingGenerator implements DummyTrainingGenerator {

	@Override
	public Training generate() {
		Training training = new Training();
		training.setDescription("Description");
		training.setLength(1L);
		training.setName("Name");
		return training;
	}

}
