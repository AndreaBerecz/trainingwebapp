package com.epam.tp.training.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.epam.tp.training.dao.TrainingDao;
import com.epam.tp.training.dao.domain.Training;
import com.epam.tp.training.service.generator.DummyTrainingGenerator;

@Component
@Transactional
public class DefaultTrainingService implements TrainingService {

	@Autowired
	private TrainingDao trainingDao;

	@Autowired
	@Qualifier("random")
	private DummyTrainingGenerator randomTrainingGenerator;

	@Autowired
	@Qualifier("prototype")
	private DummyTrainingGenerator prototypeTrainingGenerator;

	@Override
	public Iterable<Training> getTrainings() {
		return trainingDao.getTrainings();
	}

	@Override
	public void createDummyTrainings(int count) {
		boolean firstCreation = !getTrainings().iterator().hasNext();
		for (int i = 0; i < count; i++) {
			Training training = generateTraining(firstCreation);
			trainingDao.addTraining(training);
			firstCreation = false;
		}
	}

	private Training generateTraining(boolean prototype) {
		Training training;
		if (prototype) {
			training = prototypeTrainingGenerator.generate();
		} else {
			training = randomTrainingGenerator.generate();
		}
		return training;
	}

	@Override
	public Training getTraining(Long id) {
		return trainingDao.getTraining(id);
	}

	@Override
	public void modifyTraining(Long id, String name, String description, long version) {
		trainingDao.modifyTraining(id, name, description, version);
	}
	
}
