package com.epam.tp.training.service.generator;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.epam.tp.training.dao.domain.Training;

@Component
@Qualifier("random")
class RandomDummyTrainingGenerator implements DummyTrainingGenerator {

    private static final String RANDOM_TRAINING_DESCRIPTION = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nMauris lacinia, leo a aliquet sollicitudin, est risus pharetra eros, quis rutrum nisi est in tellus.\nAenean sem elit, bibendum et nisi vitae, efficitur efficitur mi.\nMaecenas molestie ultrices velit, tincidunt consequat ipsum suscipit eu.";
    private static final String RANDOM_TRAINING_PREFIX = "Training #";

    @Override
    public Training generate() {
        Training training = new Training();
        training.setName(RANDOM_TRAINING_PREFIX + RandomStringUtils.randomAlphabetic(6));
        training.setDescription(RANDOM_TRAINING_DESCRIPTION);
        training.setLength(Long.valueOf(RandomUtils.nextInt(30)));
        return training;
    }

}
