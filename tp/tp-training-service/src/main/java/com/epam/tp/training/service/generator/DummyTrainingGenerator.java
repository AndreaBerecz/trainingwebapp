package com.epam.tp.training.service.generator;

import com.epam.tp.training.dao.domain.Training;

public interface DummyTrainingGenerator {

	public Training generate();
	
}
