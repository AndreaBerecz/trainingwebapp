package com.epam.tp.feedback.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.epam.tp.feedback.entity.FeedbackEntity;
import com.epam.tp.training.entity.TrainingEntity;

public interface FeedbackRepository extends CrudRepository<FeedbackEntity, Long> {

    List<FeedbackEntity> findByTrainingEntityId(Long id);

}