package com.epam.tp.feedback.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.epam.tp.training.entity.TrainingEntity;

/**
 * Feedback entity.
 */
@Entity
@Table(name = "Feedback")
public class FeedbackEntity {

	private static final int MAX_DESCRIPTION_LENGTH = 1000;

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @ManyToOne
    private TrainingEntity trainingEntity;

    @Column(name = "description", length = MAX_DESCRIPTION_LENGTH)
    private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TrainingEntity getTrainingEntity() {
		return trainingEntity;
	}

	public void setTrainingEntity(TrainingEntity trainingEntity) {
		this.trainingEntity = trainingEntity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
    
    
    
}
