package com.epam.tp.feedback.web.transformer;

import com.epam.tp.feedback.dao.domain.Feedback;
import com.epam.tp.feedback.web.domain.FeedbackView;

public interface FeedbackToFeedbackViewTransformer {

	FeedbackView transform(Feedback feedback);

}
