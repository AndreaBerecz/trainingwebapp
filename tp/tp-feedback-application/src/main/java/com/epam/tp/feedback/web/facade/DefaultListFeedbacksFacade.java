package com.epam.tp.feedback.web.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.tp.feedback.dao.domain.Feedback;
import com.epam.tp.feedback.service.FeedbackService;
import com.epam.tp.feedback.web.domain.FeedbackView;
import com.epam.tp.feedback.web.transformer.FeedbackToFeedbackViewTransformer;

@Component
class DefaultListFeedbacksFacade implements ManageFeedbacksFacade {

    private static final int DUMMY_COUNT = 10;

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private FeedbackToFeedbackViewTransformer feedbackToFeedbackViewTransformer;

    @Override
    public Iterable<FeedbackView> getFeedbacks(Long trainingId) {
        Iterable<Feedback> feedbacks = feedbackService.getFeedbacks(trainingId);
        List<FeedbackView> feedbackViews = transform(feedbacks);
        return feedbackViews;
    }

    private List<FeedbackView> transform(Iterable<Feedback> feedbacks) {
        List<FeedbackView> feedbackViews = new ArrayList<FeedbackView>();
        for (Feedback feedback : feedbacks) {
            feedbackViews.add(feedbackToFeedbackViewTransformer.transform(feedback));
        }
        return feedbackViews;
    }

    @Override
    public FeedbackView getFeedback(Long id) {
        Feedback feedback = feedbackService.getFeedback(id);
        return feedbackToFeedbackViewTransformer.transform(feedback);
    }

	@Override
	public void addFeedback(Long trainingId, String description) {
		feedbackService.addFeedback(trainingId, description);
		
	}

}
