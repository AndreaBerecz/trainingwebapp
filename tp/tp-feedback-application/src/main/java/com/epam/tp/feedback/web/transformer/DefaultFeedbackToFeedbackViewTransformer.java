package com.epam.tp.feedback.web.transformer;

import org.springframework.stereotype.Component;

import com.epam.tp.feedback.dao.domain.Feedback;
import com.epam.tp.feedback.web.domain.FeedbackView;

@Component
class DefaultFeedbackToFeedbackViewTransformer implements FeedbackToFeedbackViewTransformer {

    @Override
    public FeedbackView transform(Feedback feedback) {
        FeedbackView feedbackView = new FeedbackView();
        feedbackView.setId(feedback.getId());
        feedbackView.setTrainingId((feedback.getTraining()).getId());
        feedbackView.setDescription(feedback.getDescription());
        return feedbackView;
    }

}