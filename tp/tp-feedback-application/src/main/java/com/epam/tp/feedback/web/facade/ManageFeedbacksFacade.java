package com.epam.tp.feedback.web.facade;

import com.epam.tp.feedback.web.domain.FeedbackView;

public interface ManageFeedbacksFacade {

    Iterable<FeedbackView> getFeedbacks(Long trainingId);

    FeedbackView getFeedback(Long id);

    void addFeedback(Long trainingId, String description);

}
