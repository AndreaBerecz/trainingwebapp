package com.epam.tp.feedback.dao;

import com.epam.tp.feedback.dao.domain.Feedback;

public interface FeedbackDao {

    Iterable<Feedback> getFeedbacks(Long trainingId);

    void addFeedback(Feedback feedback);

    Feedback getFeedback(Long id);

	void addFeedback(Long trainingId, String description);

}
