package com.epam.tp.feedback.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.tp.feedback.dao.FeedbackDao;
import com.epam.tp.feedback.dao.domain.Feedback;
import com.epam.tp.feedback.dao.transformer.FeedbackEntityToFeedbackTransformer;
import com.epam.tp.feedback.dao.transformer.FeedbackToFeedbackEntityTransformer;
import com.epam.tp.feedback.entity.FeedbackEntity;
import com.epam.tp.feedback.repository.FeedbackRepository;
import com.epam.tp.training.entity.TrainingEntity;
import com.epam.tp.training.repository.TrainingRepository;

@Component
class DefaultFeedbackDao implements FeedbackDao {

    @Autowired
    private FeedbackRepository feedbackRepository;
    
    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private FeedbackEntityToFeedbackTransformer feedbackEntityToFeedbackTransformer;

    @Autowired
    private FeedbackToFeedbackEntityTransformer feedbackToFeedbackEntityTransformer;

    @Override
    public void addFeedback(Feedback feedback) {
        FeedbackEntity feedbackEntity = feedbackToFeedbackEntityTransformer.transform(feedback);
        feedbackRepository.save(feedbackEntity);
    }

    @Override
    public Iterable<Feedback> getFeedbacks(Long trainingId) {
        Iterable<FeedbackEntity> feedbackEntitys = feedbackRepository.findByTrainingEntityId(trainingId);
        List<Feedback> result = transform(feedbackEntitys);
        return result;
    }

    private List<Feedback> transform(Iterable<FeedbackEntity> feedbackEntitys) {
        List<Feedback> result = new ArrayList<Feedback>();
        for (FeedbackEntity feedbackEntity : feedbackEntitys) {
            result.add(feedbackEntityToFeedbackTransformer.transform(feedbackEntity));
        }
        return result;
    }

    @Override
    public Feedback getFeedback(Long id) {
        FeedbackEntity feedback = feedbackRepository.findOne(id);
        if (feedback == null) {
            throw new IllegalArgumentException("No feedback with this id: " + id);
        }
        return feedbackEntityToFeedbackTransformer.transform(feedback);
    }

	@Override
	public void addFeedback(Long trainingId, String description) {
		TrainingEntity training = trainingRepository.findOne(trainingId);
		FeedbackEntity feedback = new FeedbackEntity();
		feedback.setTrainingEntity(training);
		feedback.setDescription(description);
		feedbackRepository.save(feedback);
	}


}
