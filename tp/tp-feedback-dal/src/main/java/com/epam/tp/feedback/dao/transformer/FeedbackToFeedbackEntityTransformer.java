package com.epam.tp.feedback.dao.transformer;

import com.epam.tp.feedback.dao.domain.Feedback;
import com.epam.tp.feedback.entity.FeedbackEntity;

public interface FeedbackToFeedbackEntityTransformer {

	FeedbackEntity transform(Feedback feedback);

}
