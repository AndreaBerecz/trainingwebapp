package com.epam.tp.feedback.dao.domain;

import com.epam.tp.training.dao.domain.Training;

public class Feedback {

    private Long id;

    private Training training;

    private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
    
    
}
