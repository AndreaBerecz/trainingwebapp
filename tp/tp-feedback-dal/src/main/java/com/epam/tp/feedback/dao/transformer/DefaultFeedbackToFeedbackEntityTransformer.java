package com.epam.tp.feedback.dao.transformer;

import org.springframework.stereotype.Component;

import com.epam.tp.feedback.dao.domain.Feedback;
import com.epam.tp.feedback.entity.FeedbackEntity;
import com.epam.tp.training.dao.transformer.TrainingToTrainingEntityTransformer;

@Component
class DefaultFeedbackToFeedbackEntityTransformer implements FeedbackToFeedbackEntityTransformer {
	private TrainingToTrainingEntityTransformer trainingToTrainingEntityTransformer;
	
    @Override
    public FeedbackEntity transform(Feedback feedback) {
    	FeedbackEntity feedbackEntity = new FeedbackEntity();
    	feedbackEntity.setId(feedback.getId());
    	feedbackEntity.setTrainingEntity(trainingToTrainingEntityTransformer.transform(feedback.getTraining()));
    	feedbackEntity.setDescription(feedback.getDescription());
        return feedbackEntity;
    }

}

