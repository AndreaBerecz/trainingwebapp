package com.epam.tp.feedback.dao.transformer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.tp.feedback.dao.domain.Feedback;
import com.epam.tp.feedback.entity.FeedbackEntity;
import com.epam.tp.training.dao.transformer.TrainingEntityToTrainingTransformer;

@Component
class DefaultFeedbackEntityToFeedbackTransformer implements FeedbackEntityToFeedbackTransformer {

	@Autowired
	private TrainingEntityToTrainingTransformer trainingEntityToTrainingTransformer;
    
	@Override
    public Feedback transform(FeedbackEntity feedbackEntity) {
    	Feedback feedback = new Feedback();
        feedback.setId(feedbackEntity.getId());
        feedback.setTraining(trainingEntityToTrainingTransformer.transform(feedbackEntity.getTrainingEntity()));
        feedback.setDescription(feedbackEntity.getDescription());
        return feedback;
    }

}
