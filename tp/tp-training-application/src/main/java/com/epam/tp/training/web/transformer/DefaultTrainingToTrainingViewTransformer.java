package com.epam.tp.training.web.transformer;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.springframework.stereotype.Component;

import com.epam.tp.training.dao.domain.Training;
import com.epam.tp.training.web.domain.TrainingView;

@Component
class DefaultTrainingToTrainingViewTransformer implements TrainingToTrainingViewTransformer {

    @Override
    public TrainingView transform(Training training) {
        NumberFormat numberFormat = new DecimalFormat();
        String length = numberFormat.format(training.getLength());
        TrainingView trainingView = new TrainingView();
        trainingView.setId(training.getId());
        trainingView.setName(training.getName());
        trainingView.setDescription(training.getDescription());
        trainingView.setLength(length);
        trainingView.setVersion(training.getVersion());
        return trainingView;
    }

}
