package com.epam.tp.training.web.transformer;

import com.epam.tp.training.dao.domain.Training;
import com.epam.tp.training.web.domain.TrainingView;

/**
 * Transform Training to TrainingView.
 */
public interface TrainingToTrainingViewTransformer {

    /**
     * Transformation.
     *
     * @param training to transform
     * @return transformed training
     */
    TrainingView transform(Training training);

}
