package com.epam.tp.training.web.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.tp.training.dao.domain.Training;
import com.epam.tp.training.service.TrainingService;
import com.epam.tp.training.web.domain.TrainingView;
import com.epam.tp.training.web.transformer.TrainingToTrainingViewTransformer;

@Component
class DefaultListTrainingsFacade implements ManageTrainingsFacade {

    private static final int DUMMY_COUNT = 10;

    @Autowired
    private TrainingService trainingService;

    @Autowired
    private TrainingToTrainingViewTransformer trainingToTrainingViewTransformer;

    @Override
    public Iterable<TrainingView> getTrainings() {
        Iterable<Training> trainings = trainingService.getTrainings();
        List<TrainingView> trainingViews = transform(trainings);
        return trainingViews;
    }

    private List<TrainingView> transform(Iterable<Training> trainings) {
        List<TrainingView> trainingViews = new ArrayList<TrainingView>();
        for (Training training : trainings) {
            trainingViews.add(trainingToTrainingViewTransformer.transform(training));
        }
        return trainingViews;
    }

    @Override
    public void createDummyTrainings() {
        trainingService.createDummyTrainings(DUMMY_COUNT);
    }

    @Override
    public TrainingView getTraining(Long id) {
        Training training = trainingService.getTraining(id);
        return trainingToTrainingViewTransformer.transform(training);
    }

    @Override
    public void modifyTraining(Long id, String name, String description, long version) {
        trainingService.modifyTraining(id, name, description, version);
    }

}
