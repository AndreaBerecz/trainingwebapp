package com.epam.tp.training.web.facade;

import com.epam.tp.training.web.domain.TrainingView;

/**
 * Facade for training related functionalities.
 */
public interface ManageTrainingsFacade {

    /**
     * Populate database with dummy trainings.
     */
    void createDummyTrainings();

    /**
     * Query all the trainings.
     *
     * @return Trainings
     */
    Iterable<TrainingView> getTrainings();

    /**
     * Get training with the specified id.
     *
     * @param id id
     * @return The specified training
     */
    TrainingView getTraining(Long id);

    /**
     * Modify an existing training.
     *
     * @param id id
     * @param name name
     * @param description description
     */
    void modifyTraining(Long id, String name, String description, long version);

}
