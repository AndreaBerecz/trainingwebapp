package com.epam.tp.training.web.domain;

/**
 * View model for a Training.
 */
public class TrainingView {

    private long id;

    private String name;

    private String description;

    private String length;
    
    private long version;

    /**
     * Default constructor.
     */
    public TrainingView() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

    
}
