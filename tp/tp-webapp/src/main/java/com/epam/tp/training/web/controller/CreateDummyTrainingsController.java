package com.epam.tp.training.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

import com.epam.tp.training.web.facade.ManageTrainingsFacade;

@Controller
class CreateDummyTrainingsController {

    public static final String CREATE_DUMMY_TRAININGS = "/createDummy";

    @Autowired
    private ManageTrainingsFacade listTrainingsFacade;

    @RequestMapping(value = CREATE_DUMMY_TRAININGS, method = RequestMethod.POST)
    public RedirectView createDummyTrainings() {
        listTrainingsFacade.createDummyTrainings();
        return new RedirectView(ListTrainingsController.LIST_TRAININGS_VIEW);
    }

}
