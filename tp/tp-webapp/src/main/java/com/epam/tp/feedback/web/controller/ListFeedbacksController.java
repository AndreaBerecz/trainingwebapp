package com.epam.tp.feedback.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.tp.feedback.web.domain.FeedbackView;
import com.epam.tp.feedback.web.facade.ManageFeedbacksFacade;
import com.epam.tp.training.web.domain.TrainingView;
import com.epam.tp.training.web.facade.ManageTrainingsFacade;

@Controller
class ListFeedbacksController {

    public static final String FEEDBACKS_MODEL_NAME = "feedbacks";
    public static final String LIST_FEEDBACKS_VIEW = "feedbacks";
    public static final String SHOW_FEEDBACKS = "/feedbacks";
    

    @Autowired
    private ManageFeedbacksFacade listFeedbacksFacade;
    
    @Autowired
    private ManageTrainingsFacade listTrainingsFacade;

    @RequestMapping(value = SHOW_FEEDBACKS, method = RequestMethod.GET)
    public ModelAndView showFeedbacks(Long id) {
        Iterable<FeedbackView> feedbacks = listFeedbacksFacade.getFeedbacks(id);
        return new ModelAndView(LIST_FEEDBACKS_VIEW, FEEDBACKS_MODEL_NAME, feedbacks);
    }

   
}
