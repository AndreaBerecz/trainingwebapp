package com.epam.tp.feedback.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.epam.tp.feedback.web.facade.ManageFeedbacksFacade;
import com.epam.tp.training.web.facade.ManageTrainingsFacade;

@Controller
class EditFeedbackController {

    public static final String FEEDBACK_MODEL_NAME = "feedback";
    public static final String NEW_FEEDBACK_VIEW = "new-feedback";
    public static final String NEW_FEEDBACK = "/feedback";

    @Autowired
    private ManageFeedbacksFacade listFeedbacksFacade;

    @Autowired
    private ManageTrainingsFacade listTrainingsFacade;
    
    @RequestMapping(value = NEW_FEEDBACK, method = RequestMethod.GET)
    public ModelAndView newFeedback(Long id) {
    	
        return new ModelAndView(NEW_FEEDBACK_VIEW, "id", id);
    }

    @RequestMapping(value = NEW_FEEDBACK, method = RequestMethod.POST)
    public RedirectView addFeedback(Long trainingId, String description) {
    	listFeedbacksFacade.addFeedback(trainingId, description);
        return new RedirectView("/trainings", true);
    }

}
