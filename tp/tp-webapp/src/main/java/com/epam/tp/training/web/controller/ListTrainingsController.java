package com.epam.tp.training.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.tp.training.web.domain.TrainingView;
import com.epam.tp.training.web.facade.ManageTrainingsFacade;

@Controller
class ListTrainingsController {

    public static final String TRAININGS_MODEL_NAME = "trainings";
    public static final String LIST_TRAININGS_VIEW = "trainings";
    public static final String SHOW_TRAININGS = "/trainings";

    @Autowired
    private ManageTrainingsFacade listTrainingsFacade;

    @RequestMapping(value = SHOW_TRAININGS, method = RequestMethod.GET)
    public ModelAndView showTrainings() {
        Iterable<TrainingView> trainings = listTrainingsFacade.getTrainings();
        return new ModelAndView(LIST_TRAININGS_VIEW, TRAININGS_MODEL_NAME, trainings);
    }

}
