package com.epam.tp.training.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.epam.tp.training.web.facade.ManageTrainingsFacade;

@Component
class StartupDatabasePopulator implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private ManageTrainingsFacade listTrainingsFacade;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        listTrainingsFacade.createDummyTrainings();
    }

}
