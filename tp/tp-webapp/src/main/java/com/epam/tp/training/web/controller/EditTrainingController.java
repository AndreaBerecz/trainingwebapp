package com.epam.tp.training.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.epam.tp.training.web.domain.TrainingView;
import com.epam.tp.training.web.facade.ManageTrainingsFacade;

@Controller
class EditTrainingController {

    public static final String TRAINING_MODEL_NAME = "training";
    public static final String EDIT_TRAINING_VIEW = "edit-training";
    public static final String EDIT_TRAINING = "/training";

    
    @Autowired
    private ManageTrainingsFacade listTrainingsFacade;

    @RequestMapping(value = EDIT_TRAINING, method = RequestMethod.GET)
    public ModelAndView editTraining(Long id) {
        TrainingView training = listTrainingsFacade.getTraining(id);
        return new ModelAndView(EDIT_TRAINING_VIEW, TRAINING_MODEL_NAME, training);
    }

    @RequestMapping(value = EDIT_TRAINING, method = RequestMethod.POST)
    public RedirectView modifyTraining(Long id, String name, String description, long version) {
        listTrainingsFacade.modifyTraining(id, name, description, version);
        return new RedirectView(ListTrainingsController.SHOW_TRAININGS, true);
    }

}
