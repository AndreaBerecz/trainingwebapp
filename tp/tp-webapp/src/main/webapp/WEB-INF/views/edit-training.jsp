<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Trainings</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

</head>
<body>
	<div class="container">
	    <div class="row row-centered">
			<form method="post">
				<div class="form-group">
					<label for="name">
						Name: <input class="form-control" type="text" name="name" value="${training.name}"> 
					</label>
				</div>
				<div class="form-group">
					<label for="description">
						Description: <textarea class="form-control" rows="20" cols="100" name="description">${training.description}</textarea>
					</label>
				</div>
				<div class="form-group">
					<input class="btn btn-default" type="submit" value="Save" />
					<input name="version" type="hidden" value="${training.version}" />
				</div>
			</form>
		</div>
	</div>
	
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>