<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Trainings</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

</head>
<body>
	<div class="container">
	    <div class="row row-centered">
			<table class="table table-striped table-bordered">
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th>Name</th>
					<th>Length</th>
					<th>Description</th>
				</tr>
				<c:forEach items="${trainings}" var="training">
					<tr>
						<td><a href="<c:url value="training?id=${training.id}"/>">Edit</a></td>
						<td><a href="<c:url value="feedback?id=${training.id}"/>">New feedback</a></td>
						<td><a href="<c:url value="feedbacks?id=${training.id}"/>">View feedbacks</a></td>
						<td><c:out value="${training.name}" /></td>
						<td><c:out value="${training.length}" /></td>
						<td><pre><c:out value="${training.description}" /></pre></td>
						<td><c:out value="${training.version}" /></td>
					</tr>
				</c:forEach>
			</table>
		
			<form action="<c:url value="createDummy" />" method="post">
				<input class="btn" type="submit" value="Add more random trainings" />
			</form>
		</div>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

</body>
</html>