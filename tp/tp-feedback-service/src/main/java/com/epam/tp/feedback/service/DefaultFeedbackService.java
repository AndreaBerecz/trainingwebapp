package com.epam.tp.feedback.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.epam.tp.feedback.dao.FeedbackDao;
import com.epam.tp.feedback.dao.domain.Feedback;

@Component
@Transactional
public class DefaultFeedbackService implements FeedbackService {

	@Autowired
	private FeedbackDao feedbackDao;

	@Override
	public Iterable<Feedback> getFeedbacks(Long trainingId) {
		return feedbackDao.getFeedbacks(trainingId);
	}

	@Override
	public Feedback getFeedback(Long id) {
		return feedbackDao.getFeedback(id);
	}

	@Override
	public void addFeedback(Long trainingId, String description) {
		feedbackDao.addFeedback(trainingId, description);
		
	}
}