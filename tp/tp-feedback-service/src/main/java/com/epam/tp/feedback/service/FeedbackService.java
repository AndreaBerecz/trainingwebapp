package com.epam.tp.feedback.service;

import com.epam.tp.feedback.dao.domain.Feedback;

public interface FeedbackService {

	Iterable<Feedback> getFeedbacks(Long trainingId);

	Feedback getFeedback(Long id);

	void addFeedback(Long trainingId, String description);

}
